package jwtmiddleware

import (
	"crypto/rsa"
	"fmt"
	"net/http"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/context"
	"github.com/unrolled/render"
)

// Middleware ...
type Middleware struct {
	verifyKey *rsa.PublicKey
	key       string
	value     string
}

// New is a struct that has a ServeHTTP method
func New(publicKey *rsa.PublicKey, key string, value string) *Middleware {
	return &Middleware{publicKey, key, value}
}

// The middleware handler
func (l *Middleware) ServeHTTP(w http.ResponseWriter, req *http.Request, next http.HandlerFunc) {
	render := render.New()

	// Use the specified token extractor to extract a token from the request
	head, err := FromAuthHeader(req)

	token, err := jwt.Parse(head, func(token *jwt.Token) (interface{}, error) {
		// Don't forget to validate the alg is what you expect:
		return l.verifyKey, nil
	})

	if err == nil && token.Valid {
		context.Set(req, "token", token)
		next(w, req)
	} else {
		render.JSON(w, http.StatusUnauthorized, map[string]string{"code": "403", "reason": "Unauthorized"})
	}
}

// FromAuthHeader is a "TokenExtractor" that takes a give request and extracts
// the JWT token from the Authorization header.
func FromAuthHeader(r *http.Request) (string, error) {
	authHeader := r.Header.Get("Authorization")
	if authHeader == "" {
		return "", nil // No error, just no token
	}

	// TODO: Make this a bit more robust, parsing-wise
	authHeaderParts := strings.Split(authHeader, " ")
	if len(authHeaderParts) != 2 || strings.ToLower(authHeaderParts[0]) != "bearer" {
		return "", fmt.Errorf("Authorization header format must be Bearer {token}")
	}

	return authHeaderParts[1], nil
}
